<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UsersTest extends TestCase
{
use RefreshDatabase, WithFaker;

    public function test_should_return_one_user()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $response = $this->getJson(route('api.v1.users.show', $user));
        $response->assertExactJson([
            'data' => [
                'type' => 'users',
                'id' => (string) $user->getRouteKey(),
                'attributes' => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'created_at' => $user->created_at->toJSON(),
                    'updated_at' => $user->updated_at->toJSON(),
                ],
                'links' => [
                    'self' => route('api.v1.users.show', $user)
                ]
            ]
        ]);
    }

    function test_should_return_all_users()
    {
        $this->withoutExceptionHandling();
        $users = User::factory()->count(3)->create();

        $data = [];

        foreach ($users as $user) {
            $data[] = [
                'type' => 'users',
                'id' => (string) $user->getRouteKey(),
                'attributes' => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'created_at' => $user->created_at->toJSON(),
                    'updated_at' => $user->updated_at->toJSON(),
                ],
                'links' => [
                    'self' => route('api.v1.users.show', $user)
                ]
            ];
        }

        $response = $this->getJson(route('api.v1.users.index'));
        $response->assertExactJson([
            'data' => $data,
            'links' => [
                'self' => route('api.v1.users.index')
            ]
        ]);
    }
}
