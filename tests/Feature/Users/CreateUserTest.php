<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_create_user(): void
    {
       $this->withoutExceptionHandling();
       $response = $this->postJson(route('api.v1.users.register'), [
            'data' => [
                'type' => 'users',
                'attributes' => [
                    'name' => 'John Doe',
                    'email' => 'admin@admin.com',
                    'password' => Hash::make('password'),
                ]
            ]
       ]);

       $response->assertCreated();

        $user= User::first();

        $response->assertHeader(
           'Location',
           route('api.v1.users.show', $user)
       );

       $response->assertExactJson([
            'data' => [
                'type' => 'users',
                'id' => (string) $user->getRouteKey(),
                'attributes' => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'password' => $user->password,
                ],
                'links' => [
                    'self' => route('api.v1.users.show', $user)
                ]
            ]
       ]);
    }
}
