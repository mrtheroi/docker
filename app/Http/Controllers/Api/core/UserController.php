<?php

namespace App\Http\Controllers\Api\core;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\RegisterResourse;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function show(User $user): UserResource
    {
        return UserResource::make($user);
    }

    public function index(): UserCollection
    {
        return UserCollection::make(User::all());
    }

    public function register(UserRequest $request)
    {
        $user = User::create([
            'name' => $request->input('data.attributes.name'),
            'email' => $request->input('data.attributes.email'),
            'password' => Hash::make($request->input('data.attributes.password')),
        ]);
        return RegisterResourse::make($user);

    }

}
