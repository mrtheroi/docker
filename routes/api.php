<?php

use App\Http\Controllers\Api\core\UserController;
use Illuminate\Support\Facades\Route;

Route::get('users/{user}', [UserController::class, 'show'])->name('api.v1.users.show');
Route::get('users', [UserController::class, 'index'])->name('api.v1.users.index');
Route::post('users', [UserController::class, 'register'])->name('api.v1.users.register');
